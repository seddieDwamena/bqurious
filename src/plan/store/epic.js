import { EPICS_FETCH, GET_STORY_BY_ID, EPICS_STATE, FIND_EPIC, ADD_EPIC, SAVE_OR_EDIT_EPIC, SAVE_OR_EDIT_STORY,
  SINGLE_EPIC_STATE, GET_SINGLE_EPIC, ADD_EPIC_TO_LIST, GET_EPICS_SNAPSHOT, CHECK_NODE, REMOVE_EPIC,
  ADD_STORY, EDIT_EPIC, CREATE_EPIC, DELETE_EPIC, SET_NODE, DELETE_STORY } from './store_constants'
import firebase from 'firebase'
import Utils from '../../utils'

// state
const state = {
  epics: {
    data: [
    //   {
    //     'title': 'dfgdfd',
    //     'isFolder': true,
    //     'isLazy': true,
    //     'key': 'rf_403',
    //     'id': 403
    //   },
    //   {
    //     'title': 'Test Epic',
    //     'isFolder': true,
    //     'isLazy': true,
    //     'key': 'rf_440',
    //     'id': 440
    //   }
    ],
    state: 'LOADING'
  },
  currentEpic: {
    data: {},
    state: ''
  },
  currentStory: {
    data: {}
  },
  currentNode: {
    data: {}
  }
}

// getters
const getters = {
  epics: state => state.epics.data,
  epicsState: state => state.epics.state,
  currentEpic: state => state.currentEpic.data,
  currentEpicState: state => state.currentEpic.state,
  currentStory: state => state.currentStory.data,
  currentNode: state => state.currentNode.data
}

// mutations
const mutations = {
  // get all epics
  [EPICS_FETCH] (state, payload) {
    state.epics.state = state.epics.data.length === 0 ? 'NO-DATA' : 'DATA'
    state.epics.data = payload
  },
  //   get epic state
  [EPICS_STATE] (state, payload) {
    state.epics.state = payload
  },
  //   get single epic
  [SINGLE_EPIC_STATE] (state, payload) {
    state.currentEpic.state = payload
  },
  //   get single epic state
  [GET_SINGLE_EPIC] (state, payload) {
    state.currentEpic.data = payload
    state.currentNode.data = payload
    state.currentEpic.state = 'DATA'
  },
  //   add temp epic to epics list
  [ADD_EPIC] (state, payload) {
    state.epics.data.push(payload)
    state.currentEpic.data = payload
    state.currentNode.data = payload
  },
  [ADD_EPIC_TO_LIST] (state, payload) {
    state.epics.data.push(payload)
  },
  //   add temp story to epics list
  [ADD_STORY] (state, payload) {
    state.epics.data.find(epic => epic.id === state.currentEpic.data.id).children.push(payload)
    state.currentNode.data = payload
  },
  //   set current node
  [SET_NODE] (state, payload) {
    state.currentNode.data = payload
  },
  //   find current node as per tree list
  [CHECK_NODE] (state, payload) {
    var found = {}
    state.epics.data.map(epic => {
      if (epic.id === payload) {
        found = {
          ...epic,
          type: 'epic'
        }
        state.currentEpic.data = epic
      } else {
        epic.children.map(story => {
          if (story.id === payload) {
            found = {
              ...story,
              type: 'story'
            }
            state.currentEpic.data = epic
            state.currentStory.data = story
          }
        })
      }
    })
    state.currentNode.data = found
  },
  //   remove temp epic from epics list
  [REMOVE_EPIC] (state, payload) {
    var filter = state.epics.data.filter(el => el.id !== payload)
    state.epics.data = filter
    state.currentEpic.data = state.epics.data[0]
  }
}

// actions
const actions = {
// get all epics
  [EPICS_FETCH] ({ state, commit, rootGetters }, cache = true) {
    commit(EPICS_STATE, 'LOADING')
    if (cache && state.epics.data.length !== 0) {
      commit(EPICS_STATE, 'DATA')
    } else {
      return new Promise((resolve, reject) => {
        const db = firebase.firestore()
        db.collection('epics').get().then((response) => {
          console.log('response from fire', response.docs)
          var epics = []
          response.docs.forEach((doc) => {
            // // children
            // var next = []
            // db.collection('epics').doc(doc.id)
            //   .collection('children').get().then((res) => {
            //     res.docs.forEach(number => {
            //       var newStory = {
            //         ...number.data(),
            //         id: number.id,
            //         type: 'story'
            //       }
            //       next.push(newStory)
            //     })
            //   })

            var newData = {
              ...doc.data(),
              id: doc.id,
              type: 'epic'
            }
            epics.push(newData)
          })
          commit(EPICS_FETCH, epics)
          commit(GET_SINGLE_EPIC, epics[0])
          commit(EPICS_STATE, 'DATA')
        })
      })
    }
  },

  [GET_EPICS_SNAPSHOT] ({ state, commit, rootGetters }) {
    commit(EPICS_STATE, 'LOADING')
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('epics').onSnapshot((snapshot) => {
        let changes = snapshot.docChanges()
        console.log('epic snapshot', changes)
        var newList = []
        var epics = state.epics.data

        changes.forEach(change => {
          switch (change.type) {
            case 'added':
              newList.push({
                ...change.doc.data(),
                id: change.doc.id,
                type: 'epic'
              })
              break
            case 'removed':
              commit(REMOVE_EPIC, change.doc.id)
              break
            default:
              break
          }
          console.log('newLIst', newList)
          console.log('epics', epics)
          console.log('newEpics', epics)
        })
        commit(EPICS_FETCH, newList)
      })
    })
  },

  [GET_STORY_BY_ID] ({ state, commit, rootGetters }, id) {
    commit(EPICS_STATE, 'LOADING')
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      db.collection('stories').where('epicId', '==', 'dgjRpqjdrwSCdbrk6blb').get()
        .then((response) => {
          console.log('response from fire', response.docs)
          var newEpic = {
            ...state.currentEpic.data,
            children: []
          }
          response.docs.forEach((doc) => {
            var newData = {
              ...doc.data(),
              id: doc.id
            }
            newEpic.children.push(newData)
          })
          console.log('new children', newEpic)

          var filtered = state.epics.data.filter(epic => epic.id !== id)
          var addedFil = filtered.push(newEpic)
          console.log('new epic filtered', filtered)
          commit(EPICS_FETCH, filtered)
          commit(GET_SINGLE_EPIC, newEpic)
        })
    //   db.collection('epics').get().then((response) => {
    //     console.log('response from fire', response.docs)
    //     var epics = []
    //     response.docs.forEach((doc) => {
    //       var newData = {
    //         ...doc.data(),
    //         id: doc.id
    //       }
    //       epics.push(newData)
    //     })
    //     commit(EPICS_FETCH, epics)
    //     commit(GET_SINGLE_EPIC, epics[0])
    //     commit(EPICS_STATE, 'DATA')
    //   })
    })
  },

  [FIND_EPIC] ({ state, commit }, id) {
    var sp = state.epics.data.find(epic => epic.id === id)
    commit(GET_SINGLE_EPIC, sp)
  },
  //   get single epic
  [GET_SINGLE_EPIC] ({ state, commit, rootGetters }, id) {
    commit(SINGLE_EPIC_STATE, 'LOADING')
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      db.collection('epics').doc(id).get()
        .then((response) => {
          var doc = {
            ...response.data(),
            id: id
          }
          commit(GET_SINGLE_EPIC, doc)
          commit(EPICS_STATE, 'DATA')
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },
  //   create epic
  [CREATE_EPIC] ({ state, commit, dispatch }, epic) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('epics').add(epic)
        .then((response) => {
          dispatch(EPICS_FETCH, false)
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },

  //   save or edit epic
  [SAVE_OR_EDIT_EPIC] ({ state, commit, rootGetters }, epic) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      if (typeof epic.id === 'number') {
        db.collection('epics').add(epic)
          .then((response) => {
            resolve(response)
          }).catch((err) => {
            reject(err)
          })
      } else {
        db.collection('epics').doc(epic.id)
          .update(epic)
          .then((response) => {
            resolve(response)
          }).catch(err => {
            reject(err)
          })
      }
    })
  },
  //   save or edit story
  [SAVE_OR_EDIT_STORY] ({ state, commit, rootGetters }, story) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      var currentEpic = state.currentEpic.data
      currentEpic.children.splice(currentEpic.children.findIndex(v => v.id === story.id), 1)
      currentEpic.children.push(story)
      db.collection('epics').doc(currentEpic.id)
        .update(currentEpic)
        .then((response) => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  //   delete story
  [DELETE_STORY] ({ state, commit, rootGetters }, story) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      var currentEpic = state.currentEpic.data
      currentEpic.children.splice(currentEpic.children.findIndex(v => v.id === story), 1)

      db.collection('epics').doc(currentEpic.id)
        .update(currentEpic)
        .then((response) => {
          commit(GET_SINGLE_EPIC, currentEpic)
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  //   edit epic
  [EDIT_EPIC] ({ state, commit, rootGetters }, epic) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('epics').doc(epic.id).update(epic)
        .then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  //   delete epic
  [DELETE_EPIC] ({ state, commit, dispatch }, id) {
    console.log('delete', id)
    return new Promise((resolve, reject) => {
      if (typeof id === 'number') {
        commit(REMOVE_EPIC, id)
      } else {
        const db = firebase.firestore()
        db.collection('epics').doc(id).delete()
          .then((response) => {
            dispatch(EPICS_FETCH, false)
            resolve(response)
          }).catch((error) => {
            reject(error)
          })
      }
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
