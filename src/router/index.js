import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/Login'
import Client from '@/pages/Client'
import Dashboard from '@/dashboard/pages/Dashboard'
import Sprint from '@/plan/pages/Sprint'
import UserStory from '@/plan/pages/UserStory'
import ObjectContainer from '@/design/pages/ObjectContainer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Client',
      component: Client,
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/sprint',
          name: 'Sprint',
          component: Sprint
        },
        {
          path: '/user-story',
          name: 'UserStory',
          component: UserStory
        },
        {
          path: '/object-container',
          name: 'ObjectContainer',
          component: ObjectContainer
        }
      ]
    }
  ]
})
