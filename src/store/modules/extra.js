import { GET_FOOTER_ACTIONS, SET_FOOTER_ACTIONS } from './store_constants'

// state
const state = {
  footer: {
    actions: [],
    isExport: false
  },
  user: {
    data: {
      name: 'blah'
    }
  },
  applications: ['web', 'SAP Windows GUI', 'Mobile Native', 'Mobile Native'],
  objects: ['link', 'image', 'label', 'listItem', 'list', 'div', 'span', 'table', 'row', 'cell', 'table header', 'button', 'checkbox',
    'password', 'radio', 'submit', 'textbox', 'reset', 'hidden', 'imagesubmitbutton', 'select', 'option', 'textarea', 'heading', 'font', 'area']
}

// getters
const getters = {
  footer: state => state.footer,
  applications: state => state.applications,
  objects: state => state.objects,
  user: state => state.user.data
}

// mutations
const mutations = {
  [SET_FOOTER_ACTIONS] (state, payload) {
    state.footer = payload
  }
}

// actions
const actions = {
  [GET_FOOTER_ACTIONS] ({ commit }, data) {
    commit(SET_FOOTER_ACTIONS, data)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
