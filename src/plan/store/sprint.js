import { SPRINTS_FETCH, GET_SPRINTS_SNAPSHOT, FIND_SPRINT, ADD_SPRINT, SPRINTS_STATE, SINGLE_SPRINT_STATE,
  GET_SINGLE_SPRINT, EDIT_SPRINT, REMOVE_SPRINT, CREATE_SPRINT, DELETE_SPRINT, SAVE_OR_EDIT_SPRINT } from './store_constants'
import firebase from 'firebase'
// import moment from 'moment'

// state
const state = {
  sprints: {
    data: [
    //   {
    //     'title': 'sprint01',
    //     'isLazy': false,
    //     'key': 'rel_222',
    //     'id': 222
    //   },
    //   {
    //     'title': 'Test 01',
    //     'isLazy': false,
    //     'key': 'rel_237',
    //     'id': 237
    //   },
    //   {
    //     'title': 'Test Sprint',
    //     'isLazy': false,
    //     'key': 'rel_242',
    //     'id': 242
    //   }
    ],
    state: 'LOADING',
    details: {
      Id: 222,
      AppId: 1,
      CompleteDate: null,
      EndDate: null,
      ExternalId: null,
      ExternalUrl: null,
      InActive: false,
      ReleaseDesc: null,
      ReleaseVer: 'sprint01',
      StartDate: null
    }
  },
  currentSprint: {
    data: [],
    state: ''
  }
}

// getters
const getters = {
  sprints: state => state.sprints.data,
  sprintsState: state => state.sprints.state,
  sprintDetails: state => state.sprints.details,
  currentSprint: state => state.currentSprint.data,
  currentSprintState: state => state.currentSprint.state
}

// mutations
const mutations = {
  [SPRINTS_FETCH] (state, payload) {
    state.sprints.state = 'DATA'
    state.sprints.data = payload
  },
  [SPRINTS_STATE] (state, payload) {
    state.sprints.state = payload
  },
  [SINGLE_SPRINT_STATE] (state, payload) {
    state.currentSprint.state = payload
  },
  [GET_SINGLE_SPRINT] (state, payload) {
    state.currentSprint.data = payload
    state.currentSprint.state = 'DATA'
  },
  [ADD_SPRINT] (state, payload) {
    state.sprints.data.push(payload)
    state.currentSprint.data = payload
  },
  [REMOVE_SPRINT] (state, payload) {
    var filter = state.sprints.data.filter(el => el.id !== payload)
    state.sprints.data = filter
    state.currentSprint.data = state.sprints.data[0]
    console.log('filter', filter)
  }
}

// actions
const actions = {
  // get all sprints
  [SPRINTS_FETCH] ({ state, commit, rootGetters }, cache = true) {
    commit(SPRINTS_STATE, 'LOADING')
    if (cache && state.sprints.data.length !== 0) {
      commit(SPRINTS_STATE, 'DATA')
    } else {
      return new Promise((resolve, reject) => {
        const db = firebase.firestore()
        db.collection('sprints').get().then((response) => {
          console.log('response from fire', response.docs)
          var sprints = []
          response.docs.forEach((doc) => {
            var newData = {
              ...doc.data(),
              //   startDate: moment(doc.data().startDate).format('YYYY-MM-DD'),
              id: doc.id
            }
            sprints.push(newData)
          })
          commit(SPRINTS_FETCH, sprints)
          commit(GET_SINGLE_SPRINT, sprints[0])
          commit(SPRINTS_STATE, 'DATA')
        })
      })
    }
  },
  [GET_SPRINTS_SNAPSHOT] ({ state, commit, rootGetters }, cache = true) {
    commit(SPRINTS_STATE, 'LOADING')
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('sprints').onSnapshot((snapshot) => {
        let changes = snapshot.docChanges()
        console.log('sprint snapshot', changes)
        var newList = []
        changes.forEach(change => {
          switch (change.type) {
            case 'added':
              newList.push({
                ...change.doc.data(),
                id: change.doc.id
              })
              break
            default:
              break
          }
          console.log('newLIst', newList)
        })
        commit(SPRINTS_FETCH, newList)
      })
    })
  },
  [FIND_SPRINT] ({ state, commit, rootGetters }, id) {
    var sp = state.sprints.data.find(sprint => sprint.id === id)
    commit(GET_SINGLE_SPRINT, sp)
  },
  //   get single sprint
  [GET_SINGLE_SPRINT] ({ state, commit, rootGetters }, id) {
    commit(SINGLE_SPRINT_STATE, 'LOADING')
    console.log('node to be delted', id)
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      db.collection('sprints').doc(id).get()
        .then((response) => {
          var doc = {
            ...response.data(),
            id: id
          }
          console.log('class', doc)
          commit(GET_SINGLE_SPRINT, doc)
          commit(SPRINTS_STATE, 'DATA')
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },
  //   create sprint
  [CREATE_SPRINT] ({ state, commit, rootGetters }, sprint) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('sprints').add(sprint)
        .then((response) => {
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },
  //   save or edit
  [SAVE_OR_EDIT_SPRINT] ({ state, commit, rootGetters }, sprint) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      sprint.startDate = sprint.startDate ? sprint.startDate.toString() : ''
      sprint.endDate = sprint.endDate ? sprint.endDate.toString() : ''
      if (typeof sprint.id === 'number') {
        db.collection('sprints').add(sprint)
          .then((response) => {
            resolve(response)
          }).catch((err) => {
            reject(err)
          })
      } else {
        db.collection('sprints').doc(sprint.id)
          .update(sprint)
          .then((response) => {
            resolve(response)
          }).catch(err => {
            reject(err)
          })
      }

    //   db.collection('sprints').doc(sprint.id).get()
    //     .then((docSnapshot) => {
    //       console.log('sprint', sprint)
    //       console.log('doc snapshot', docSnapshot)
    //       if (docSnapshot.exists) {

    //       } else {

    //       }
    //     })
    })
  },
  //   edit sprint
  [EDIT_SPRINT] ({ state, commit, rootGetters }, sprint) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('sprints').doc(sprint.id).update(sprint)
        .then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  //   delete sprint
  [DELETE_SPRINT] ({ commit }, id) {
    return new Promise((resolve, reject) => {
      if (typeof id === 'number') {
        commit(REMOVE_SPRINT, id)
      } else {
        const db = firebase.firestore()
        db.collection('sprints').doc(id).delete()
          .then((response) => {
            resolve(response)
          }).catch((error) => {
            reject(error)
          })
      }
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
