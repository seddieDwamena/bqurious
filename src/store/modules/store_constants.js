// epics
export const EPICS_FETCH = 'getEpics'
export const EPICS_STATE = 'getEpicsState'
export const GET_EPICS_URI = '/epics'

// sprints
export const SPRINTS_FETCH = 'getSprints'
export const SPRINTS_STATE = 'getSprintsState'
export const GET_SPRINTS_URI = '/sprints'

// footer
export const GET_FOOTER_ACTIONS = 'getFooterActions'
export const SET_FOOTER_ACTIONS = 'setFooterActions'
