// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import '@/assets/styles/app.scss'
import Vuex from 'vuex'
import store from './store/store.js'
import components from './component'
import VueChartist from 'vue-chartist'

Vue.use(Antd)
Vue.use(Vuex)
Vue.use(VueChartist)

Vue.component(components)

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAHmBfGn9vKnHeEidXckPkFw7HJskAKwrU',
  authDomain: 'ember-library-app-f71bc.firebaseapp.com',
  databaseURL: 'https://ember-library-app-f71bc.firebaseio.com',
  projectId: 'ember-library-app-f71bc',
  storageBucket: 'ember-library-app-f71bc.appspot.com',
  messagingSenderId: '869125958956'
}
firebase.initializeApp(config)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
