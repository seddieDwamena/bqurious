import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
import epics from '../plan/store/epic'
import sprints from '../plan/store/sprint'
import applications from '../design/store/application'
import extra from './modules/extra'
// import taskStore from '../task/task-store'
// import groupStore from '../task/group-store'
// import userStore from '../user/user-store'
// import roleStore from '../role/role-store'
// import employeeStore from '../employee/employee-store'
// import salepointStore from '../salespoint/salespoint-store'

// axios.defaults.baseURL = 'http://18.212.143.172/bq/3/2/1/'
// axios.defaults.baseURL = 'http://localhost:62415/api/'

Vue.use(Vuex)
Vue.config.devtools = true

export default new Vuex.Store({
  modules: {
    epics,
    sprints,
    applications,
    extra
  }
})
