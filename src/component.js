import Vue from 'vue'
import Division from '@/components/Division'
import SidePanel from '@/components/SidePanel'
import BreadCrumb from '@/components/BreadCrumb'

// import LineChart from '@/components/LineChart.js'
import BoxButton from '@/components/BoxButton'
import EmptyTree from '@/components/EmptyTree'

Vue.component('bread-crumb', BreadCrumb)
Vue.component('division', Division)
Vue.component('side-panel', SidePanel)
// Vue.component('line-chart', LineChart)
Vue.component('box-button', BoxButton)
Vue.component('is-empty', EmptyTree)

export default {
  Division: Division,
  SidePanel: SidePanel,
  BreadCrumb: BreadCrumb,
  BoxButton: BoxButton,
//   LineChart: LineChart,
  EmptyTree: EmptyTree
}
