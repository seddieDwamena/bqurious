import { APPS_FETCH, GET_APPS_URI, GET_OBJECT_BY_ID, APPS_STATE, FIND_APP, ADD_APP, SAVE_OR_EDIT_APP,
  SINGLE_APP_STATE, GET_SINGLE_APP, SAVE_OR_EDIT_OBJECT, DELETE_OBJECT, CHECK_APP_NODE, GET_SINGLE_OBJECT, ADD_OBJECT, EDIT_APP, CREATE_APP, DELETE_APP,
  REMOVE_APP } from './store_constants'
import firebase from 'firebase'

// state
const state = {
  apps: {
    data: [],
    state: 'LOADING'
  },
  currentApp: {
    data: [],
    state: ''
  },
  currentObject: {
    data: []
  },
  currentAppNode: {
    data: []
  }
}

// getters
const getters = {
  apps: state => state.apps.data,
  appsState: state => state.apps.state,
  currentApp: state => state.currentApp.data,
  currentAppState: state => state.currentApp.state,
  currentObject: state => state.currentObject.data,
  currentAppNode: state => state.currentAppNode.data
}

// mutations
const mutations = {
  [APPS_FETCH] (state, payload) {
    state.apps.state = state.apps.data.length === 0 ? 'NO-DATA' : 'DATA'
    state.apps.data = payload
  },
  [APPS_STATE] (state, payload) {
    state.apps.state = payload
  },
  [SINGLE_APP_STATE] (state, payload) {
    state.currentApp.state = payload
  },
  [GET_SINGLE_APP] (state, payload) {
    state.currentApp.data = payload
    state.currentAppNode.data = payload
    state.currentApp.state = 'DATA'
  },
  [ADD_APP] (state, payload) {
    state.apps.data.push(payload)
    state.currentApp.data = payload
    state.currentAppNode.data = payload
  },
  [ADD_OBJECT] (state, payload) {
    state.apps.data.find(app => app.id === state.currentApp.data.id).children.push(payload)
    state.currentAppNode.data = payload
  },
  [CHECK_APP_NODE] (state, payload) {
    var found = {}
    state.apps.data.map(app => {
      if (app.id === payload) {
        found = {
          ...app,
          type: 'app'
        }
        state.currentApp.data = app
      } else {
        app.children.map(story => {
          if (story.id === payload) {
            found = {
              ...story,
              type: 'object'
            }
            state.currentApp.data = app
          }
        })
      }
    })
    state.currentAppNode.data = found
  },
  //   remove temp epic from epics list
  [REMOVE_APP] (state, payload) {
    var filter = state.apps.data.filter(el => el.id !== payload)
    state.apss.data = filter
    state.currentApp.data = state.apps.data[0]
  }
}

// actions
const actions = {
// get all apps
  [APPS_FETCH] ({ state, commit, rootGetters }, cache = true) {
    commit(APPS_STATE, 'LOADING')
    if (cache && state.apps.data.length !== 0) {
      commit(APPS_STATE, 'DATA')
    } else {
      return new Promise((resolve, reject) => {
        const db = firebase.firestore()
        db.collection('applications').get().then((response) => {
          console.log('response from fire', response.docs)
          var apps = []
          response.docs.forEach((doc) => {
            // children
            var next = []
            db.collection('applications').doc(doc.id)
              .collection('children').get().then((res) => {
                res.docs.forEach(number => {
                  var newStory = {
                    ...number.data(),
                    id: number.id,
                    type: 'object'
                  }
                  next.push(newStory)
                })
              })

            var newData = {
              ...doc.data(),
              id: doc.id,
              children: next,
              type: 'app'
            }
            apps.push(newData)
          })
          commit(APPS_FETCH, apps)
          commit(GET_SINGLE_APP, apps[0])
          commit(APPS_STATE, 'DATA')
        })
      })
    }
  },
  [FIND_APP] ({ state, commit }, id) {
    var sp = state.apps.data.find(app => app.id === id)
    commit(GET_SINGLE_APP, sp)
  },
  //   get single app
  [GET_SINGLE_APP] ({ state, commit, rootGetters }, id) {
    commit(SINGLE_APP_STATE, 'LOADING')
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      db.collection('applications').doc(id).get()
        .then((response) => {
          var doc = {
            ...response.data(),
            id: id
          }
          commit(GET_SINGLE_APP, doc)
          commit(APPS_STATE, 'DATA')
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },
  //   create app
  [CREATE_APP] ({ state, commit, rootGetters }, app) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('applications').add(app)
        .then((response) => {
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
    })
  },

  //   save or edit
  [SAVE_OR_EDIT_APP] ({ state, commit, rootGetters }, app) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      if (typeof app.id === 'number') {
        db.collection('applications').add(app)
          .then((response) => {
            resolve(response)
          }).catch((err) => {
            reject(err)
          })
      } else {
        db.collection('applications').doc(app.id)
          .update(app)
          .then((response) => {
            resolve(response)
          }).catch(err => {
            reject(err)
          })
      }
    })
  },

  //   save or edit object
  [SAVE_OR_EDIT_OBJECT] ({ state, commit, dispatch }, object) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      var currentApp = state.currentApp.data
      currentApp.children.splice(currentApp.children.findIndex(v => v.id === object.id), 1)
      currentApp.children.push(object)
      db.collection('applications').doc(currentApp.id)
        .update(currentApp)
        .then((response) => {
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },
  //   delete object
  [DELETE_OBJECT] ({ state, commit, rootGetters }, object) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()

      var currentApp = state.currentApp.data
      currentApp.children.splice(currentApp.children.findIndex(v => v.id === object), 1)

      db.collection('applications').doc(currentApp.id)
        .update(currentApp)
        .then((response) => {
          commit(GET_SINGLE_APP, currentApp)
          resolve(response)
        }).catch(err => {
          reject(err)
        })
    })
  },

  //   edit app
  [EDIT_APP] ({ state, commit, rootGetters }, app) {
    return new Promise((resolve, reject) => {
      const db = firebase.firestore()
      db.collection('applications').doc(app.id).update(app)
        .then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(err)
        })
    })
  },
  //   delete app
  [DELETE_APP] ({ state, commit, dispatch }, id) {
    console.log('delete', id)
    return new Promise((resolve, reject) => {
      if (typeof id === 'number') {
        commit(REMOVE_APP, id)
      } else {
        const db = firebase.firestore()
        db.collection('applications').doc(id).delete()
          .then((response) => {
            dispatch(APPS_FETCH, false)
            resolve(response)
          }).catch((error) => {
            reject(error)
          })
      }
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
